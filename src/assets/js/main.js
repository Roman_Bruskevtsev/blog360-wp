(function($) {

    var position = $(window).scrollTop();

    function headerScroll(){
        let header = $('header'),
            mobileNav = $('.mobile__nav'),
            sideBar = $('.mobile__navigation'),
            windowScroll = $(window).scrollTop();
        
        if( windowScroll > header.height() ){
            header.addClass('background');
        } else {
            header.removeClass('background');
        }

        if( windowScroll > 50 ){
            mobileNav.addClass('background');
            sideBar.addClass('top');
        } else {
            mobileNav.removeClass('background');
            sideBar.removeClass('top');
        }

        var scroll = $(window).scrollTop();
        if(scroll > position) {
            $('header').addClass('hide');
        } else {
            $('header').removeClass('hide');
        }
        position = scroll;
    }

    $(document).ready(function(){
        $('.preloader__wrapper .logo').addClass('show');
    });

    $(window).on('scroll', function() {
        headerScroll();
    });

    $(window).on('load', function() {
        headerScroll();
        /*Preloader*/
        setTimeout(function(){
            $('.preloader__wrapper .logo').addClass('done');
        }, 2000);
        setTimeout(function(){
            $('.preloader__wrapper').addClass('loaded');
        }, 3000);
        /*Mobile menu*/
        $('.mobile__btn').on('click', function(){
            $('.mobile__navigation').addClass('show__menu');
            $('.mobile__nav').addClass('hide__menu');
        })
        $('.close__mobile').on('click', function(){
            $('.mobile__navigation').removeClass('show__menu');
            $('.mobile__nav').removeClass('hide__menu');
        });
        /*Posts slider*/
        if ($('.posts__slider').length) {
            var $postsSlider = {
                infinite: true,
                autoplay: true,
                autoplaySpeed: 8000,
                speed: 1500,
                vertical: true,
                centerMode: true,
                centerPadding: '30px',
                slidesToShow: 3,
                dots: false,
                arrows: true,
                slidesToScroll: 1,
                verticalSwiping: true,
                focusOnSelect: true,
                asNavFor: '.detail__slider',
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            arrows: false,
                            slidesToShow: 1,
                            centerPadding: '0px',
                        }
                    }
                ]
            }

            var $detailSlider = {
                infinite: true,
                autoplay: false,
                slidesToShow: 1,
                dots: false,
                arrows: false,
                slidesToScroll: 1,
                fade: true,
                asNavFor: '.posts__slider'
            }

            $('.posts__slider').slick($postsSlider);
            $('.detail__slider').slick($detailSlider);

            $('.post__item').on('click', function(){
                $(this).addClass('show__info');
                $('.post__information').addClass('show__slider');
                $('.logo__block').addClass('hide');
            });

            var e = 1;
            $('.posts__block, .post__information').hover(
                function(){
                    e = 1;
                },
                function(){
                    e = 0;
                }
            );

            $('body').on('click', function(){
                if(e == 0) {
                    $('.post__information').removeClass('show__slider');
                }
            });
            // $('.posts__slider').on('afterChange', function(slick, currentSlide){
            //     $(this).removeClass('show__info');
            //     $('.post__information').removeClass('show__slider');
            // });
        }
        
        /*Posts slider*/
        if ($('.blog__slider').length) {
            $('.blog__slider').slick({
                infinite: true,
                autoplay: false,
                slidesToShow: 1,
                dots: false,
                speed: 1200,
                slidesToScroll: 1,
                prevArrow: $('.prev__slide'),
                nextArrow: $('.next__slide')
            });
        }
        /*Posts slider*/
        if ($('.post__slider').length) {
            $('.post__slider').slick({
                infinite: true,
                autoplay: false,
                slidesToShow: 1,
                dots: false,
                slidesToScroll: 1,
                prevArrow: $('.slider__wrapper').find('.prev__slide'),
                nextArrow: $('.slider__wrapper').find('.next__slide')
            });
        }

        /*Paralax on scroll*/
        $('.paroller__horizontal').paroller({
            factor: -0.2,
            factorXs: -0.05,
            type: 'foreground',
            direction: 'horizontal'
        });

        $('.paroller__vertical').paroller({
            factor: 0.2,
            factorXs: 0.25,
            type: 'foreground',
            direction: 'vertical'
        });
        /*Google map*/
        var marker = [],
            infowindow = [],
            map, image = $('.map__wrapper').attr('data-marker');

        function addMarker(location, name, contentstr) {

            marker[name] = new google.maps.Marker({
                position: location,
                map: map,
                icon: image
            });
            marker[name].setMap(map);

            // infowindow[name] = new google.maps.InfoWindow({
            //     content: contentstr
            // });

            // google.maps.event.addListener(marker[name], 'click', function() {
            //     infowindow[name].open(map, marker[name]);
            // });
        }

        function initialize() {
            var lat = $('.map__wrapper').attr("data-lat");
            var lng = $('.map__wrapper').attr("data-lng");
            var mark_name = $('.map__wrapper').attr("data-name");
            var mark_str = $('.map__wrapper').attr("data-str");

            var myLatlng = new google.maps.LatLng(lat, lng);

            var setZoom = parseInt($('.zoom__field').val());

            addMarker(myLatlng, mark_name, mark_str);

            var styles = [
                {
                    "featureType": "administrative",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#a9a28e"
                        }
                    ]
                },
                {
                    "featureType": "administrative.neighborhood",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "lightness": "8"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#f2f2f2"
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#f8f5ea"
                        }
                    ]
                },
                {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#fffcf5"
                        }
                    ]
                },
                {
                    "featureType": "landscape.natural.landcover",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#f6e8c5"
                        }
                    ]
                },
                {
                    "featureType": "landscape.natural.terrain",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#d1c8af"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#b6d8c0"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 45
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#f5efe2"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#46bcec"
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#b6d8c0"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "visibility": "off"
                        },
                        {
                            "color": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#14626e"
                        }
                    ]
                }
            ];

            var styledMap = new google.maps.StyledMapType(styles, { name: "Styled Map" });

            var mapOptions = {
                zoom: setZoom,
                disableDefaultUI: true,
                scrollwheel: false,
                zoomControl: false,
                streetViewControl: false,
                center: myLatlng
            };
            map = new google.maps.Map(document.getElementById("google__map"), mapOptions);

            map.mapTypes.set('map_style', styledMap);
            map.setMapTypeId('map_style');

            $('.marker__details').each(function() {
                var mark_lat = $(this).attr('data-lat');
                var mark_lng = $(this).attr('data-lng');
                var this_index = $('.marker__details').index(this);
                var mark_name = 'template_marker_' + this_index;
                var mark_locat = new google.maps.LatLng(mark_lat, mark_lng);
                var mark_title = $(this).attr('data-title');
                var mark_description = $(this).attr('data-str');
                var mark_link = $(this).attr('data-href');
                var mark_str = '<h4>'+ mark_title +'</h4><p>'+ mark_description +'</p><a href="'+ mark_link +'"><span class="border__top"></span><span class="text">More</span><span class="border__bottom"></span></a>';
                addMarker(mark_locat, mark_name, mark_str);
            });

            $('.post__item').on('click', function(){
                let postLat = $(this).attr('data-lat'),
                    postLng = $(this).attr('data-lng');

                let latlng = new google.maps.LatLng(postLat, postLng);
                map.setCenter(latlng);
                map.setZoom(12);
                // setTimeout(function(){
                //     $('.range__slider').slider('value', 12);
                // }, 300);
            });
            $('.to__map').on('click', function(){
                let mapBlock = $('.map__wrapper'),
                    mapLat = mapBlock.data('lat'),
                    mapLng = mapBlock.data('lng'),
                    mapZoom = parseInt($('.zoom__field').val()),
                    mapLatLng = new google.maps.LatLng(mapLat, mapLng);

                    console.log(mapZoom);
                    map.setCenter(mapLatLng);
                    map.setZoom(mapZoom);
            });
        }

        if ($('.map__wrapper').length) {
            setTimeout(function() {
                initialize();
            }, 500);
        }

        /*Input slider*/
        let mapZoomMax = parseInt($('.zoom__field').attr('max')),
            mapZoomMin = parseInt($('.zoom__field').attr('min')),
            mapZoomDefault = parseInt($('.zoom__field').attr('value'));
        $('.range__slider').slider({
            orientation: "vertical",
            step: 1,
            min: mapZoomMin,
            max: mapZoomMax,
            value: mapZoomDefault,
            change: function( event, ui ){
                let zoom = ui.value;
                $('.zoom__field').val(zoom);
                initialize();
            }
        });

        /*Video player*/
        $('.blog__slider .play__icon').on('click', function(){
            let thisVideoBlock = $(this).closest('.wrapper').find('.video__block'),
                thisVideoFrame = thisVideoBlock.find('iframe')[0];
            $('body').addClass('play__video');
            thisVideoBlock.addClass('show__video');

            setTimeout(function(){
                thisVideoFrame.contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
            }, 300);
        });
        $('.blog__slider .stop__icon').on('click', function(){
            let thisVideoBlock = $(this).closest('.wrapper').find('.video__block'),
                thisVideoFrame = thisVideoBlock.find('iframe')[0];
            $('body').removeClass('play__video');
            thisVideoBlock.removeClass('show__video');

            setTimeout(function(){
                thisVideoFrame.contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
            }, 300);
        });

        $('.post__preview .play__icon').on('click', function(){
            let thisVideoBlock = $(this).closest('.post__preview').find('.video__block'),
                thisVideoFrame = thisVideoBlock.find('iframe')[0];
            $('body').addClass('play__video');
            thisVideoBlock.addClass('show__video');

            setTimeout(function(){
                thisVideoFrame.contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
            }, 300);
        });
        $('.post__preview .stop__icon').on('click', function(){
            let thisVideoBlock = $(this).closest('.post__preview').find('.video__block'),
                thisVideoFrame = thisVideoBlock.find('iframe')[0];
            $('body').removeClass('play__video');
            thisVideoBlock.removeClass('show__video');

            setTimeout(function(){
                thisVideoFrame.contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
            }, 300);
        });

        $('.video__wrapper .play__icon').on('click', function(){
            let thisVideoBlock = $(this).closest('.video__wrapper'),
                thisVideoFrame = thisVideoBlock.find('iframe')[0];
            thisVideoBlock.addClass('show__video');

            setTimeout(function(){
                thisVideoFrame.contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
            }, 300);
        });
        $('.video__wrapper .stop__icon').on('click', function(){
            let thisVideoBlock = $(this).closest('.video__wrapper'),
                thisVideoFrame = thisVideoBlock.find('iframe')[0];
            thisVideoBlock.removeClass('show__video');

            setTimeout(function(){
                thisVideoFrame.contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
            }, 300);
        });

        /*Blog loader*/
        function showBlog(){
            if ( $('.blog__load').length && !( $('.blog__wrapper').hasClass('show') ) ) {
                var windowScroll    = $(window).scrollTop(),
                    loadLine        = $('.load__line');
                if(windowScroll > 10){
                    loadLine.addClass('load');
                    setTimeout( function(){
                        loadLine.removeClass('load');
                        $('.blog__wrapper').addClass('show').slideDown(500);
                    }, 1000);
                }
            }
        }

        if ( $('.blog__wrapper').length) {
            var maxPageCount,
                cat,
                searchKey,
                page = 1;

            $('.more__post button').on('click', function(){
                maxPageCount = parseInt($('.blog__wrapper .posts__list').data('max-page')),
                cat = $('.blog__wrapper .posts__list').data('cat'),
                searchKey = $('.blog__wrapper .posts__list').data('search');
                $(this).closest('.more__post').addClass('load');
                $('.blog__wrapper .posts__list').addClass('load');

                $.post( ajaxurl, {
                    'action'        : 'blog360_load_posts',
                    'category_id'   : cat,
                    'page'          : page,
                    'search_key'    : searchKey
                    
                })
                .done(function(response) {
                    $('.blog__wrapper .posts__list').append(response);
                    page++;
                    if(maxPageCount <= page) {
                        $('.more__post button').addClass('disable');
                    } else {
                        $('.more__post button').removeClass('disable');
                    }
                    $('.blog__wrapper .posts__list').removeClass('load');
                    $('.more__post').removeClass('load');
                });

            });
        }

        showBlog();
        $(window).on('resize scroll', function(){
            showBlog();
        });
        /*Share post*/
        if($('.additional__information').length){
            var shareBlock          = $('.additional__information'),
                shareBlockTopCon    = shareBlock.offset().top;
        }
        function shareInit(){
            if($('.additional__information').length){
                var windowHeight    = $(window).height(), 
                    windowScroll    = $(window).scrollTop(),
                    documentHeight  = $('html, body').outerHeight(),
                    footerHeight    = $('footer').outerHeight(),
                    shareBlock      = $('.additional__information'),
                    sectionMargin   = parseInt($('.post__content').css('margin-top').replace('px','')),
                    shareBlockTop   = shareBlock.offset().top,
                    postHeight      = $('.post__content').outerHeight(),
                    postTop         = $('.post__content').offset().top;

                // console.log(postTop);
                if( windowScroll + sectionMargin > shareBlockTopCon ){
                    shareBlock.addClass('fixed');
                } else {
                    shareBlock.removeClass('fixed');
                }

                if( windowScroll + shareBlock.outerHeight() + 50 > postHeight + postTop ){
                    shareBlock.addClass('absolute');
                } else {
                    shareBlock.removeClass('absolute');
                }
            }
        }
        shareInit();
        $(window).on('resize scroll', function(){
            shareInit();
        });
    });
})(jQuery);