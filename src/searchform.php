<?php
/**
 *
 * @package WordPress
 * @subpackage Blog360
 * @since 1.0
 * @version 1.0
 */

?>

<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>
<div class="search__block float-right">
    <form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    	<div class="search__field">
            <i class="icon search"></i>
            <input type="search" id="<?php echo $unique_id; ?>" class="form__field search" placeholder="<?php echo SEARCH; ?>" value="<?php echo get_search_query(); ?>" name="s" />
        </div>
    	<button type="submit" class="hidden"></button>
    </form>
</div>