<?php $post_thumb = ( get_field('post_banner') ) ? ' style="background-image: url('.get_field('post_banner').');"' : ''; ?>
<div class="wrapper"<?php echo $post_thumb; ?>>
    <?php if( get_field('post_video_id') ) { ?>
    <div class="video">
        <div class="play__icon"></div>
    </div>
    <div class="video__block">
        <div class="stop__icon">
            <span class="border__top"></span>
            <span class="close"></span>
            <span class="border__bottom"></span>
        </div>
        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/<?php the_field('post_video_id'); ?>?enablejsapi=1&version=3&playerapiid=ytplayer" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>
    <?php } ?>
    <div class="content">
        <h2><?php the_title(); ?></h2>
        <p><?php the_field('desription'); ?></p>
        <a href="<?php the_permalink(); ?>" class="btn yellow__border">
            <span class="border__top"></span>
            <span class="text"><?php echo MORE; ?></span>
            <span class="border__bottom"></span>
        </a>
    </div>
</div>