<?php $post_thumb = ( get_the_post_thumbnail( get_the_ID() ) ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'large' ).');"' : ''; ?>
<a href="<?php the_permalink(); ?>" class="article">
    <div class="thumbnail"<?php echo $post_thumb; ?>></div>
    <div class="title"></div>
    <h3><?php the_title(); ?></h3>
</a>