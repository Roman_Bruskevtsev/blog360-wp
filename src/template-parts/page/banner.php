<?php $background = ( get_field('image') ) ? ' style="background-image: url('.get_field('image').')"' : ''; ?>
<div class="page__banner"<?php echo $background; ?>>
    <?php if( get_field('title') ) { ?>
        <div class="title">
            <h2><?php the_field('title'); ?></h2>
        </div>
    <?php } ?>
</div>