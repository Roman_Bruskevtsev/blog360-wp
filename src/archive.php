<?php
/**
 *
 * @package WordPress
 * @subpackage Blog360
 * @since 1.0
 * @version 1.0
 */
get_header(); 

$posts_per_page = (int) get_option('posts_per_page');
$category = get_category( get_query_var( 'cat' ) );

$all_posts_args = array(
    'posts_per_page'        => $posts_per_page,
    'orderby'               => 'date',
    'post_status'           => 'publish',
    'cat'					=> $category->term_id
);

$all_query = new WP_Query( $all_posts_args ); 
$max_pages = (int) $all_query->max_num_pages;

?>
<div class="page__title">
	<div class="container">
	    <div class="row">
	        <div class="col">
	            <div class="section__title">
	                <h4><?php echo CATEGORY; ?><?php echo $category->name; ?></h4>
	            </div>
	        </div>
	    </div>
	</div>
</div>

<?php if ( $all_query->have_posts() ) { ?>
    <div class="blog__wrapper">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="nav__row">
                        <?php if( has_nav_menu('blog-categories-1') ){ 
                            $menu_name_1 = 'blog-categories-1';
                            $locations_1 = get_nav_menu_locations();
                            $menu_id_1 = $locations_1[ $menu_name_1 ] ;
                            $menu_object_1 = wp_get_nav_menu_object($menu_id_1);
                            ?>
                            <div class="dropdown__nav float-left">
                                <div class="title">
                                    <span class="text"><?php echo $menu_object_1->name; ?></span>
                                    <span class="icon"></span>
                                </div>
                                <?php wp_nav_menu( array(
                                    'theme_location'        => 'blog-categories-1',
                                    'container'             => 'nav'
                                ) ); ?>
                            </div>
                        <?php } 
                        if( has_nav_menu('blog-categories-2') ){ 
                            $menu_name_2 = 'blog-categories-2';
                            $locations_2 = get_nav_menu_locations();
                            $menu_id_2 = $locations_1[ $menu_name_2 ] ;
                            $menu_object_2 = wp_get_nav_menu_object($menu_id_2);
                            ?>
                            <div class="dropdown__nav float-left">
                                <div class="title">
                                    <span class="text"><?php echo $menu_object_2->name; ?></span>
                                    <span class="icon"></span>
                                </div>
                                <?php wp_nav_menu( array(
                                    'theme_location'        => 'blog-categories-2',
                                    'container'             => 'nav'
                                ) ); ?>
                            </div>
                        <?php } 
                        get_search_form(); ?>
                    </div>
                    <div class="posts__wrapper">
                        <div class="posts__list" data-page="1" data-max-page="<?php echo $max_pages; ?>" data-cat="<?php echo $category->term_id; ?>">
                            <?php while ( $all_query->have_posts() ) { $all_query->the_post();
                                get_template_part( 'template-parts/post/content' );
                            } ?>
                        </div>
                        <?php if ( $max_pages > 1) { ?>
                        <div class="more__post">
                            <button class="btn dark yellow__border">
                                <span class="border__top"></span>
                                <span class="text"><?php echo MOREBLOGS; ?></span>
                                <span class="border__bottom">
                            </button>
                            <div class="load__icon"></div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php 
wp_reset_postdata(); 
} else { ?>

<?php } ?>
<?php get_footer();