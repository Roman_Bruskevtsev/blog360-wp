<?php
/**
 *
 * @package WordPress
 * @subpackage Blog360
 * @since 1.0
 * @version 1.0
 */
get_header(); 

get_template_part( 'template-parts/post/content-single' );

$next_post = get_next_post();
$prev_post = get_previous_post();
if( have_rows('content') ): ?>
    <div class="post__content">
        <div class="d-none d-xl-block additional__information">
            <div class="date"><?php echo get_the_date(); ?></div>
            <div class="sharing">
                <span class="text"><?php echo SHARE; ?></span>
                <ul>
                    <li>
                        <span class="twitter st-custom-button" data-network="twitter" data-short-url="<?php the_permalink();?>" data-title="<?php the_title(); ?>" data-description="<?php echo strip_tags(get_the_excerpt()); ?>" data-image="<?php echo get_the_post_thumbnail_url(); ?>">
                        </span>
                    </li>
                    <li>
                        <span class="facebook st-custom-button" data-network="facebook" data-short-url="<?php the_permalink();?>" data-title="<?php the_title(); ?>" data-description="<?php echo strip_tags(get_the_excerpt()); ?>" data-image="<?php echo get_the_post_thumbnail_url(); ?>">
                        </span>
                    </li>
                    <?php if( get_field('instagram', 'option') ){ ?>
                    <li>
                        <a href="<?php the_field('instagram', 'option'); ?>" target="_blank" class="instagram"></a>
                    </li>
                    <?php } ?>
                </ul>
            </div>
            <?php echo blog360_partners(); ?>
        </div>
    <?php while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'content_editor' ):
            get_template_part( 'inc/acf-content/content-editor' );
        elseif( get_row_layout() == 'full_image_block' ): 
            get_template_part( 'inc/acf-content/full-image-block' );
        elseif( get_row_layout() == 'image_slider' ): 
            get_template_part( 'inc/acf-content/image-slider' );
        elseif( get_row_layout() == 'video_block' ): 
            get_template_part( 'inc/acf-content/video-block' );
        endif;
    endwhile; ?>
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="d-block d-xl-none additional__information__bottom">
                        <div class="date"><?php echo get_the_date(); ?></div>
                        <div class="sharing">
                            <span class="text"><?php echo SHARE; ?></span>
                            <ul>
                                <li>
                                    <span class="twitter st-custom-button" data-network="twitter" data-short-url="<?php the_permalink();?>" data-title="<?php the_title(); ?>" data-description="<?php echo strip_tags(get_the_excerpt()); ?>" data-image="<?php echo get_the_post_thumbnail_url(); ?>">
                                    </span>
                                </li>
                                <li>
                                    <span class="facebook st-custom-button" data-network="facebook" data-short-url="<?php the_permalink();?>" data-title="<?php the_title(); ?>" data-description="<?php echo strip_tags(get_the_excerpt()); ?>" data-image="<?php echo get_the_post_thumbnail_url(); ?>">
                                    </span>
                                </li>
                                <?php if( get_field('instagram', 'option') ){ ?>
                                <li>
                                    <a href="<?php the_field('instagram', 'option'); ?>" target="_blank" class="instagram"></a>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <?php if( have_rows('partners', 'option') ): ?>
                        <div class="partners">
                            <span class="text"><?php echo PARTNERS; ?></span>
                            <ul>
                                <?php while ( have_rows('partners', 'option') ) : the_row(); ?>
                                <li>
                                    <?php if( get_sub_field('link') ) { ?><a href="<?php the_sub_field('link'); ?>" target="_blank"><?php } ?>
                                        <?php if( get_sub_field('logo') ) { ?><img src="<?php echo get_sub_field('logo')['url']; ?>" alt=""><?php } ?>
                                    <?php if( get_sub_field('link') ) { ?></a><?php } ?>
                                </li>
                                <?php endwhile; ?>
                            </ul>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="posts__navigation">
        <?php if($prev_post) { 
        $prev_thumbnail = (get_the_post_thumbnail_url($prev_post->ID)) ? ' style="background-image: url('.get_the_post_thumbnail_url($prev_post->ID).')"': '';
        ?>
        <a href="<?php echo get_permalink($prev_post->ID); ?>" class="post prev">
            <div class="thumb"<?php echo $prev_thumbnail; ?>></div>
            <div class="title">
                <h4><?php echo $prev_post->post_title; ?></h4>
            </div>
        </a>
        <?php } ?>
        <?php if($next_post) { 
        $next_thumbnail = (get_the_post_thumbnail_url($prev_post->ID)) ? ' style="background-image: url('.get_the_post_thumbnail_url($next_post->ID).')"': '';
        ?>
        <a href="<?php echo get_permalink($next_post->ID); ?>" class="post next">
            <div class="thumb"<?php echo $next_thumbnail; ?>></div>
            <div class="title">
                <h4><?php echo $next_post->post_title; ?></h4>
            </div>
        </a>
        <?php } ?>
    </div>
    <?php get_template_part( 'inc/acf-content/youtube-block-option' ); ?>
<?php else :
    echo '
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="no__content">
                        <h1>'.__('Nothing to show', 'blog360').'</h1>
                    </div>
                </div>
            </div>
        </div>
    ';
endif;

get_footer();