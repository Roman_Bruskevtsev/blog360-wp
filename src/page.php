<?php
/**
 *
 * @package WordPress
 * @subpackage Blog360
 * @since 1.0
 * @version 1.0
 */
get_header(); 

if( get_field('show_page_banner') ) {
    get_template_part( 'template-parts/page/banner' );
    $padding = '';
} else {
    $padding = ' padding__top';
}

if( have_rows('content') ):
    echo '<div class="page__content'.$padding.'">';
    while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'posts_slider' ):
            get_template_part( 'inc/acf-content/posts-slider' );
        elseif( get_row_layout() == 'image_text_block' ): 
            get_template_part( 'inc/acf-content/image-text-block' );
        elseif( get_row_layout() == 'youtube_block' ): 
            get_template_part( 'inc/acf-content/youtube-block' );
        elseif( get_row_layout() == 'youtube_block_full' ): 
            get_template_part( 'inc/acf-content/youtube-block-full' );
        elseif( get_row_layout() == 'partners_block' ): 
            get_template_part( 'inc/acf-content/partners-block' );
        elseif( get_row_layout() == 'text_block_1' ): 
            get_template_part( 'inc/acf-content/text-block-1' );
        elseif( get_row_layout() == 'text_block_2' ): 
            get_template_part( 'inc/acf-content/text-block-2' );
        elseif( get_row_layout() == 'requisites_block' ): 
            get_template_part( 'inc/acf-content/requisites-block' );
        elseif( get_row_layout() == 'video_block' ): 
            get_template_part( 'inc/acf-content/video-block' );
        elseif( get_row_layout() == 'contact_information_image' ): 
            get_template_part( 'inc/acf-content/contact-information-image' );
        elseif( get_row_layout() == 'contact_block_logo' ): 
            get_template_part( 'inc/acf-content/contact-block-logo' );
        elseif( get_row_layout() == 'team_block' ): 
            get_template_part( 'inc/acf-content/team-block' );
        elseif( get_row_layout() == 'contact_block_image' ): 
            get_template_part( 'inc/acf-content/contact-block-image' );
        endif;
    endwhile;
    echo '</div>';
else :
    echo '
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="no__content">
                        <h1>'.__('Nothing to show', 'blog360').'</h1>
                    </div>
                </div>
            </div>
        </div>
    ';
endif;

get_footer();