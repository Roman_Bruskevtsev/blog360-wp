<?php if( get_sub_field('title') ) { ?>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="section__title margin__45">
                <h2><?php the_sub_field('title'); ?></h2>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<div class="container">
    <div class="row">
        <?php if( get_sub_field('image') ) { ?>
        <div class="col-2 col-sm-4 col-md-5 col-lg-7 col-xl-7">
            <div class="contact__image">
                <img src="<?php the_sub_field('image'); ?>">
            </div>
        </div>
        <?php } ?>
        <div class="col-10 col-sm-8 col-md-7 col-lg-5 col-xl-5">
            <?php if( get_sub_field('form_shortcode') ) { ?>
            <div class="contact__form paroller__vertical">
                <?php echo do_shortcode( get_sub_field('form_shortcode') ); ?>
            </div>
            <?php } ?>
        </div>
    </div>
</div>