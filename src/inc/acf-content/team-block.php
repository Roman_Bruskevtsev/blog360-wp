<?php if( get_sub_field('title') ) { ?>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="section__title text-right">
                <h2><?php the_sub_field('title'); ?></h2>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php if( have_rows('members') ): $i = 1; ?>
<div class="container">
    <?php while ( have_rows('members') ) : the_row(); ?>
    <div class="row justify-content-md-center">
        <div class="col-xl-10">
            <div class="team__member d-block d-sm-none">
                <div class="row">
                    <div class="col-12">
                        <div class="image">
                            <?php if( get_sub_field('image_1') ) { ?><div style="background-image: url('<?php the_sub_field('image_1'); ?>')" class="img image__1"></div><?php } ?>
                            <?php if( get_sub_field('image_2') ) { ?><div style="background-image: url('<?php the_sub_field('image_2'); ?>')" class="img image__2"></div><?php } ?>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="name margin__top text-left">
                            <?php if( get_sub_field('name') ) { ?><h4><?php the_sub_field('name'); ?></h4><?php } ?>
                            <?php if( get_sub_field('biography') ) { ?><p><?php the_sub_field('biography'); ?></p><?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="team__member d-none d-sm-block">
                <div class="row">
                    <?php if( ($i % 2) == 1 ){ ?>
                        <div class="col-8 col-sm-7 col-md-6 col-lg-6 col-xl-6">
                            <div class="image">
                                <?php if( get_sub_field('image_1') ) { ?><div style="background-image: url('<?php the_sub_field('image_1'); ?>')" class="img image__1"></div><?php } ?>
                                <?php if( get_sub_field('image_2') ) { ?><div style="background-image: url('<?php the_sub_field('image_2'); ?>')" class="img image__2"></div><?php } ?>
                            </div>
                        </div>
                        <div class="col-4 col-sm-5 col-md-6 col-lg-6 col-xl-6">
                            <div class="name text-left paroller__vertical">
                                <?php if( get_sub_field('name') ) { ?><h4><?php the_sub_field('name'); ?></h4><?php } ?>
                                <?php if( get_sub_field('biography') ) { ?><p><?php the_sub_field('biography'); ?></p><?php } ?>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="col-4 col-sm-5 col-md-6 col-lg-6 col-xl-6">
                            <div class="name text-right paroller__vertical">
                                <?php if( get_sub_field('name') ) { ?><h4><?php the_sub_field('name'); ?></h4><?php } ?>
                                <?php if( get_sub_field('biography') ) { ?><p><?php the_sub_field('biography'); ?></p><?php } ?>
                            </div>
                        </div>
                        <div class="col-8 col-sm-7 col-md-6 col-lg-6 col-xl-6">
                            <div class="image">
                            <?php if( get_sub_field('image_1') ) { ?><div style="background-image: url('<?php the_sub_field('image_1'); ?>')" class="img image__1"></div><?php } ?>
                            <?php if( get_sub_field('image_2') ) { ?><div style="background-image: url('<?php the_sub_field('image_2'); ?>')" class="img image__2"></div><?php } ?>
                        </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <?php $i++; endwhile; ?>
</div>
<?php endif; ?>