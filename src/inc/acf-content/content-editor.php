<?php if( get_sub_field('text') ) { ?>
<div class="container">
    <div class="row">
        <div class="col-xl-2">
        </div>
        <div class="col-xl-10">
            <div class="content">
                <?php the_sub_field('text'); ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>