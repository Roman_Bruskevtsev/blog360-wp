<?php $background = ( get_sub_field('video_preview') ) ? ' style="background-image: url('.get_sub_field('video_preview').')"' : ''; ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-12">
            <div class="content">
                <div class="video__wrapper">
                    <div class="image__preview"<?php echo $background; ?>>
                        <div class="play__icon"></div>
                        <?php if( get_sub_field('youtube_video_id') ) { ?>
                        <div class="video__block">
                            <div class="stop__icon">
                                <span class="border__top"></span>
                                <span class="close"></span>
                                <span class="border__bottom"></span>
                            </div>
                            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/<?php the_sub_field('youtube_video_id'); ?>?enablejsapi=1&version=3&playerapiid=ytplayer" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>