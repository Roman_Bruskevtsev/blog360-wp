<div class="form__block">
    <div class="container-fluid">
        <?php if( get_sub_field('title') ) { ?>
        <div class="row">
            <div class="col">
                <div class="section__title margin__45 padding__top__120">
                    <h2><?php the_sub_field('title'); ?></h2>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <?php if( get_sub_field('logo') ) { ?>
            <div class="col-md-5 col-xl-5">
                <div class="contacts__logo">
                    <img src="<?php the_sub_field('logo'); ?>" alt="">
                </div>
            </div>
            <?php } ?>
            <?php if( get_sub_field('form_shortcode') ) { ?>
            <div class="col-md-7 col-xl-7">
                <div class="contact__form__large paroller__vertical">
                    <?php echo do_shortcode( get_sub_field('form_shortcode'), false ); ?>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>