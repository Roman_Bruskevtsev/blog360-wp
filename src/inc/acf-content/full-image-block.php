<?php if( get_sub_field('image') ) { ?>
<div class="container__fluid">
    <div class="row">
        <div class="col">
            <div class="content">
                <img src="<?php the_sub_field('image'); ?>">
            </div>
        </div>
    </div>
</div>
<?php } ?>