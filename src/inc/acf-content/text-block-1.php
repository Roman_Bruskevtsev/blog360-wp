<?php if( get_sub_field('text') ) { ?>
<div class="container">
    <div class="row justify-content-end">
        <div class="col-sm-12 col-md-8 col-lg-8 col-xl-8">
            <div class="section__text margin">
                <p><?php the_sub_field('text'); ?></p>
            </div>
        </div>
    </div>
</div>
<?php } ?>