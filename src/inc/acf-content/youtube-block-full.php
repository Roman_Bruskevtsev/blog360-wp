<?php 
$style = ( get_sub_field('background_color') == 0 ) ? 'white' : 'yellow'; 
$border = ( get_sub_field('background_color') == 0 ) ? 'yellow__border' : 'white__border'; 
?>
<div class="subscribe__block <?php echo $style; ?>">
    <div class="content">
        <?php if( get_sub_field('text') ) { ?><h4><?php the_sub_field('text'); ?></h4><?php } ?>
        <?php if( get_sub_field('link') ) { ?>
        <div class="link">
            <a href="<?php the_sub_field('link'); ?>" target="_blank" class="btn <?php echo $border; ?>">
                <span class="border__top"></span>
                <?php if( get_sub_field('label') ) { ?><span class="text"><?php the_sub_field('label'); ?></span><?php } ?>
                <span class="border__bottom"> </span>
            </a>
        </div>
        <?php } ?>
    </div>
</div>