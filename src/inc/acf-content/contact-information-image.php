<div class="contacts padding">
    <?php if( get_sub_field('title') ) { ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="section__title">
                    <h2><?php the_sub_field('title'); ?></h2>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="container-fluid">
        <div class="row">
            <?php if( get_sub_field('image') ) { ?>
            <div class="col-md-7 col-xl-7">
                <div class="contacts__image">
                    <img src="<?php the_sub_field('image'); ?>" alt="">
                </div>
            </div>
            <?php } ?>
            <div class="col-md-5 col-xl-5">
                <div class="contacts__text paroller__vertical">
                    <div class="tel__block">
                        <?php if( get_sub_field('phone_1') ) { ?><a href="tel:<?php the_sub_field('phone_1'); ?>"><?php the_sub_field('phone_1'); ?></a><?php } ?>
                        <?php if( get_sub_field('phone_2') ) { ?><a href="tel:<?php the_sub_field('phone_2'); ?>"><?php the_sub_field('phone_2'); ?></a><?php } ?>
                    </div>
                    <?php if( get_sub_field('email') ) { ?>
                    <div class="email__block">
                        <a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a>
                    </div>
                    <?php } ?>
                    <div class="social__block white">
                        <nav>
                            <ul>
                                <?php if( get_field('facebook', 'option') ){ ?>
                                <li>
                                    <a href="<?php the_field('facebook', 'option'); ?>" target="_blank" class="facebook"></a>
                                </li>
                                <?php } ?>
                                <?php if( get_field('instagram', 'option') ){ ?>
                                <li>
                                    <a href="<?php the_field('instagram', 'option'); ?>" target="_blank" class="instagram"></a>
                                </li>
                                <?php } ?>
                                <?php if( get_field('youtube', 'option') ){ ?>
                                <li>
                                    <a href="<?php the_field('youtube', 'option'); ?>" target="_blank" class="youtube"></a>
                                </li>
                                <?php } ?>
                                <?php if( get_field('telegram', 'option') ){ ?>
                                <li>
                                    <a href="<?php the_field('telegram', 'option'); ?>" target="_blank" class="telegram"></a>
                                </li>
                                <?php } ?>
                                <?php if( get_field('twitter', 'option') ){ ?>
                                <li>
                                    <a href="<?php the_field('twitter', 'option'); ?>" target="_blank" class="twitter"></a>
                                </li>
                                <?php } ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>