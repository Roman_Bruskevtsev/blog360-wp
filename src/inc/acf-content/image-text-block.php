<?php if( get_sub_field('title') ) { ?>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="section__title minus__60">
                <h2><?php the_sub_field('title'); ?></h2>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<div class="container">
    <div class="row justify-content-start">
        <div class="col-lg-8 col-xl-8">
            <div class="section__text paroller__vertical">
                <?php if( get_sub_field('text') ) { ?>
                <p><?php the_sub_field('text'); ?></p>
                <?php } ?>
                <?php if( get_sub_field('button_link') ) { ?>
                <a href="<?php the_sub_field('button_link'); ?>" class="btn dark yellow__border">
                    <span class="border__top"></span>
                    <span class="text"><?php the_sub_field('button_label'); ?></span>
                    <span class="border__bottom">
                </a>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php if( get_sub_field('image') ) { ?>
    <div class="row justify-content-end">
        <div class="col-lg-8 col-xl-9">
            <div class="section__image">
                <span class="border__top"></span>
                <div class="image__block">
                    <img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('title'); ?>">
                </div>
                <span class="border__bottom"></span>
            </div>
        </div>
    </div>
    <?php } ?>
</div>