<div class="container-fluid">
    <div class="row">
        <div class="col">
            <?php if( get_sub_field('title') ) { ?>
            <div class="section__title margin__60">
                <h2><?php the_sub_field('title'); ?></h2>
            </div>
            <?php } ?>
            <div class="subscribe__block float-right">
                <div class="content">
                    <?php if( get_sub_field('text') ) { ?><h4><?php the_sub_field('text'); ?></h4><?php } ?>
                    <?php if( get_sub_field('link') ) { ?>
                    <div class="link">
                        <a href="<?php the_sub_field('link'); ?>" target="_blank" class="btn white__border">
                            <span class="border__top"></span>
                            <?php if( get_sub_field('label') ) { ?><span class="text"><?php the_sub_field('label'); ?></span><?php } ?>
                            <span class="border__bottom"> </span>
                        </a>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>