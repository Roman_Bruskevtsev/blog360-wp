<?php if( get_sub_field('title') ) { ?>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="section__title minus__80 absolute">
                <h2><?php the_sub_field('title'); ?></h2>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="partners__list">
                <?php if( have_rows('partners') ): ?>
                <div class="all__partners">
                    <?php while ( have_rows('partners') ) : the_row(); ?>
                    <div class="partner__block">
                        <?php if( get_sub_field('link') ) { ?>
                            <a href="<?php the_sub_field('link'); ?>" target="_blank">
                        <?php } else { ?>
                            <span>
                        <?php } ?>

                            <?php if( get_sub_field('logo') ) { ?><img src="<?php the_sub_field('logo'); ?>" alt=""><?php } ?>
                        
                        <?php if( get_sub_field('link') ) { ?>
                            </a>
                        <?php } else { ?>
                            </span>
                        <?php } ?>
                    </div>
                    <?php endwhile; ?>
                </div>
                <?php endif; 
                $general_partner = get_sub_field('general_partner');
                if( $general_partner ) { ?>
                <div class="general__partner">
                    <div class="partner__block">
                        <?php if( $general_partner['link'] ) { ?>
                            <a href="<?php the_sub_field('link'); ?>" target="_blank">
                        <?php } else { ?>
                            <span>
                        <?php } ?>
                            <?php if( $general_partner['logo'] ) { ?><img src="<?php echo $general_partner['logo']; ?>" alt="<?php echo $general_partner['title']; ?>"><?php } ?>
                            <?php if( $general_partner['title'] ) { ?><h6><?php echo $general_partner['title']; ?></h6><?php } ?>
                        <?php if( $general_partner['link'] ) { ?>
                            </a>
                        <?php } else { ?>
                            </span>
                        <?php } ?>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>