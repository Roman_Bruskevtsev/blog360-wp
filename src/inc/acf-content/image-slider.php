<?php
$images = get_sub_field('images');

if( $images ){ ?>
<div class="container">
    <div class="row">
        <div class="col-xl-2"></div>
        <div class="col-xl-10">
            <div class="content">
                <div class="slider__wrapper">
                    <div class="prev__slide slide__nav">
                        <span class="border__top"></span>
                        <span class="icon"></span>
                        <span class="border__bottom"></span>
                    </div>
                    <div class="post__slider">
                        <?php foreach( $images as $image ): ?>
                        <div class="slide">
                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="next__slide slide__nav">
                        <span class="border__top"></span>
                        <span class="icon"></span>
                        <span class="border__bottom"></span>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
<?php } ?>