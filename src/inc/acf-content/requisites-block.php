<div class="container">
    <div class="row">
        <?php if( get_sub_field('image') ) { ?>
        <div class="col-md-8 col-xl-8">
            <div class="donate__image">
                <img src="<?php the_sub_field('image'); ?>" alt="">
            </div>
        </div>
        <?php } ?>
        <div class="col-md-4 col-xl-4">
            <div class="donate__text paroller__vertical">
                <?php if( get_sub_field('title') ) { ?><h4><?php the_sub_field('title'); ?></h4><?php } ?>
                <?php if( have_rows('accounts') ): 
                    while ( have_rows('accounts') ) : the_row(); ?>
                    <?php if( get_sub_field('account') ) { ?><h4 class="yellow no__margin"><?php the_sub_field('account'); ?></h4><?php } ?>
                    <?php if( get_sub_field('title') ) { ?><p><?php the_sub_field('title'); ?></p><?php } ?>
                    <?php endwhile;
                endif; ?>
            </div>
        </div>
    </div>
</div>