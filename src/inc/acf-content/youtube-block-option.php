<?php 
$video = get_field('video_block', 'option');

$style = ( $video['background_color'] == 0 ) ? 'white' : 'yellow'; 
$border = ( $video['background_color'] == 0 ) ? 'yellow__border' : 'white__border'; 
?>
<div class="subscribe__block <?php echo $style; ?>">
    <div class="content">
        <?php if( $video['text'] ) { ?><h4><?php echo $video['text']; ?></h4><?php } ?>
        <?php if( $video['link'] ) { ?>
        <div class="link">
            <a href="<?php echo $video['link']; ?>" target="_blank" class="btn <?php echo $border; ?>">
                <span class="border__top"></span>
                <?php if( $video['label'] ) { ?><span class="text"><?php echo $video['label']; ?></span><?php } ?>
                <span class="border__bottom"> </span>
            </a>
        </div>
        <?php } ?>
    </div>
</div>