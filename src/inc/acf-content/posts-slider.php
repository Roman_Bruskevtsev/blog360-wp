<?php
$posts_args = array(
    'orderby'       => 'date',
    'post_status'   => 'publish'
);

$query = new WP_Query( $posts_args );

$all_posts_args = array(
    'orderby'       => 'date',
    'post_status'   => 'publish',
    'nopaging'      => 1
);

$all_query = new WP_Query( $all_posts_args ); 

?>
    <div class="map__container d-none d-lg-block">
        <input type="number" name="zoom" class="zoom__field" value="<?php the_sub_field('default_zoom'); ?>" max="<?php the_sub_field('max_zoom'); ?>" min="<?php the_sub_field('min_zoom'); ?>" hidden>
        <div id="google__map" class="map__wrapper" data-lat="<?php the_sub_field('latitude'); ?>" data-lng="<?php the_sub_field('longitude'); ?>" data-marker="<?php the_sub_field('marker'); ?>"></div>
        <div class="map__zoom">
            <div class="country__point"><?php the_sub_field('country_label'); ?></div>
            <div class="range__slider"></div>
            <div class="world__point"><?php the_sub_field('world_label'); ?></div>
        </div>
        <?php if ( $all_query->have_posts() ) : ?>
        <div class="marker__list">
            <?php while ( $all_query->have_posts() ) : $all_query->the_post(); ?>
                <div class="marker__details" data-lat="<?php the_field('latitude'); ?>" data-lng="<?php the_field('longitude'); ?>" data-title="<?php the_title(); ?>" data-str="<?php the_field('desription'); ?>" data-href="<?php the_permalink(); ?>"></div>
            <?php endwhile; ?>
        </div>
    <?php endif; ?>
    </div>
    <?php if( get_sub_field('map_logo') ) { ?>
    <div class="logo__block d-none d-lg-block">
        <img src="<?php the_sub_field('map_logo'); ?>" alt="<?php echo get_option( 'blogname' ); ?>">
    </div>
    <?php } 
    
    if ( $query->have_posts() ) : ?>
    <div class="posts__block">
        <div class="active__post"></div>
        <div class="posts__slider">
            <?php while ( $query->have_posts() ) : $query->the_post();
                $post_thumb = ( get_the_post_thumbnail( get_the_ID() ) ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'article-thumbnail' ).');"' : '';
            ?>
            <div class="slide">
                <div class="post__item" data-lat="<?php the_field('latitude'); ?>" data-lng="<?php the_field('longitude'); ?>">
                    <div class="thumbnail"<?php echo $post_thumb; ?>></div>
                    <div class="title">
                        <h5><?php the_title(); ?></h5>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
    <div class="post__information">
        <div class="detail__slider">
            <?php while ( $query->have_posts() ) : $query->the_post(); ?>
            <div class="slide">
                <div class="information">
                    <div class="to__map d-none d-lg-block"><?php _e('To all map', 'blog360'); ?></div>
                    <h3><?php the_title(); ?></h3>
                    <p><?php the_field('desription'); ?></p>
                    <a href="<?php the_permalink(); ?>" class="btn yellow__border">
                        <span class="border__top"></span>
                        <span class="text"><?php _e('More text and video', 'blog360'); ?></span>
                        <span class="border__bottom"> </span>
                    </a>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
    <?php endif; 
wp_reset_postdata(); ?>