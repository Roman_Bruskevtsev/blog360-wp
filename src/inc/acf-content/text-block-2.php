<?php if( get_sub_field('text') ) { ?>
<div class="container">
    <div class="row">
        <div class="col-xl-9">
            <div class="content">
                <p><?php the_sub_field('text'); ?></p>
            </div>
        </div>
    </div>
</div>
<?php } ?>