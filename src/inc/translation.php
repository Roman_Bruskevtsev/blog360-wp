<?php 
define("MORE",               "Більше тексту і відео");
define("MOREPOSTS",          "Більше статтей");
define("MOREBLOGS",          "Більше блогів");
define("SEARCH",             "Знайди цікавий тобі Блог");
define("SEARCHRESULTS",      "Результати пошуку: ");
define("NOTHINGTOSHOW",      "Нічого не знайдено");
define("CATEGORY",           "Категорія: ");
define("SHARE",              "Поширити");
define("PARTNERS",           "Партнери");