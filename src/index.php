<?php
/**
 *
 * @package WordPress
 * @subpackage Blog360
 * @since 1.0
 * @version 1.0
 */
get_header(); 

$blog_page = get_option('page_for_posts ');

$posts_per_page = (int) get_option('posts_per_page');

$all_posts_args = array(
    'posts_per_page'        => $posts_per_page,
    'orderby'               => 'date',
    'post_status'           => 'publish',
);

$all_query = new WP_Query( $all_posts_args ); 
$max_pages = (int) $all_query->max_num_pages;

if( $blog_page ) { 
    $slider_posts = get_field('choose_posts', $blog_page);
    if( $slider_posts ){
        $slider_posts_args = array(
            'orderby'       => 'date',
            'post_status'   => 'publish',
            'post__in'      => $slider_posts,
            'nopaging'      => 1
        );
        $slider_query = new WP_Query( $slider_posts_args ); 
        if ( $slider_query->have_posts() ) { ?>
            <div class="slider__wrapper">
                <div class="prev__slide slide__nav">
                    <span class="border__top"></span>
                    <span class="icon"></span>
                    <span class="border__bottom"></span>
                </div>
                <div class="blog__slider">
                    <?php while ( $slider_query->have_posts() ) : $slider_query->the_post(); ?>
                    <div class="slide">
                        <?php get_template_part( 'template-parts/post/content', 'slider' ); ?>
                    </div>
                    <?php endwhile; ?>
                </div>
                <div class="next__slide slide__nav">
                    <span class="border__top"></span>
                    <span class="icon"></span>
                    <span class="border__bottom"></span>
                </div>
                <div class="more__posts">
                    <div class="text"><?php echo MOREPOSTS; ?></div>
                    <span class="icon"></span>
                </div>
            </div>
            <div class="load__line">
                <div class="load__icon"></div>
            </div>
        <?php } wp_reset_postdata();
    } 
}

if ( $all_query->have_posts() ) { ?>
    <div class="blog__wrapper blog__load">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="nav__row">
                        <?php if( has_nav_menu('blog-categories-1') ){ 
                            $menu_name_1 = 'blog-categories-1';
                            $locations_1 = get_nav_menu_locations();
                            $menu_id_1 = $locations_1[ $menu_name_1 ] ;
                            $menu_object_1 = wp_get_nav_menu_object($menu_id_1);
                            ?>
                            <div class="dropdown__nav float-left">
                                <div class="title">
                                    <span class="text"><?php echo $menu_object_1->name; ?></span>
                                    <span class="icon"></span>
                                </div>
                                <?php wp_nav_menu( array(
                                    'theme_location'        => 'blog-categories-1',
                                    'container'             => 'nav'
                                ) ); ?>
                            </div>
                        <?php } 
                        if( has_nav_menu('blog-categories-2') ){ 
                            $menu_name_2 = 'blog-categories-2';
                            $locations_2 = get_nav_menu_locations();
                            $menu_id_2 = $locations_1[ $menu_name_2 ] ;
                            $menu_object_2 = wp_get_nav_menu_object($menu_id_2);
                            ?>
                            <div class="dropdown__nav float-left">
                                <div class="title">
                                    <span class="text"><?php echo $menu_object_2->name; ?></span>
                                    <span class="icon"></span>
                                </div>
                                <?php wp_nav_menu( array(
                                    'theme_location'        => 'blog-categories-2',
                                    'container'             => 'nav'
                                ) ); ?>
                            </div>
                        <?php } 
                        get_search_form(); ?>
                    </div>
                    <div class="posts__wrapper">
                        <div class="posts__list" data-page="1" data-max-page="<?php echo $max_pages; ?>" data-cat="*">
                            <?php while ( $all_query->have_posts() ) { $all_query->the_post();
                                get_template_part( 'template-parts/post/content' );
                            } ?>
                        </div>
                        <?php if ( $max_pages > 1) { ?>
                        <div class="more__post">
                            <button class="btn dark yellow__border">
                                <span class="border__top"></span>
                                <span class="text"><?php echo MOREBLOGS; ?></span>
                                <span class="border__bottom">
                            </button>
                            <div class="load__icon"></div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php 
wp_reset_postdata(); 
} else { ?>

<?php } ?>
<?php get_footer();