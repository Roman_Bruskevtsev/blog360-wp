<?php
/**
 *
 * @package WordPress
 * @subpackage Blog360
 * @since 1.0
 */

/*Translation*/
require get_template_directory() . '/inc/translation.php';

/*ACF Import*/
require get_template_directory() . '/inc/acf-import.php';

/*Theme setup*/
function blog360_setup() {
    load_theme_textdomain( 'blog360' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );

    add_image_size( 'article-thumbnail', 367, 218, true );
    add_image_size( 'team-thumbnail', 457, 543, true );
    // add_image_size( 'portfolio-thumbnail', 650, 330, true );
    // add_image_size( 'portfolio-medium', 825, 464, true );

    register_nav_menus( array(
        'main'              => __( 'Main Menu', 'blog360' ),
        'footer'            => __( 'Footer Menu', 'blog360' ),
        'blog-categories-1' => __( 'Blog Categories 1', 'blog360' ),
        'blog-categories-2' => __( 'Blog Categories 2', 'blog360' )
    ) );
}
add_action( 'after_setup_theme', 'blog360_setup' );

/*App2drive styles and scripts*/
function blog360_scripts() {
    $version = '1.0.13';

    wp_enqueue_style( 'blog360-css', get_theme_file_uri( '/assets/css/main.min.css' ), '', $version );
    wp_enqueue_style( 'blog360-style', get_stylesheet_uri() );

    wp_enqueue_script( 'slick-js', get_theme_file_uri( '/assets/js/slick.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'google-maps-key', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDOqauekT_r4JzPY18FOWN4N8DFR3hXo1U&libraries=places', array( 'jquery' ), $version, true );
    wp_enqueue_script( 'share-js', '//platform-api.sharethis.com/js/sharethis.js#property=5ae5db7fde20620011e0349f&product=custom-share-buttons', array('jquery'), $version, true );
    
    if( !is_home() ) wp_enqueue_script( 'mousewheel-smoothscroll-js', get_theme_file_uri( '/assets/js/mousewheel-smoothscroll.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'paroller-js', get_theme_file_uri( '/assets/js/jquery.paroller.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'jquery-ui', get_theme_file_uri( '/assets/js/jquery-ui.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'script-js', get_theme_file_uri( '/assets/js/main.min.js' ), array( 'jquery' ), $version, true );
}
add_action( 'wp_enqueue_scripts', 'blog360_scripts' );

/*Theme settings*/
if( function_exists('acf_add_options_page') ) {

    $general = acf_add_options_page(array(
        'page_title'    => 'Theme General Settings',
        'menu_title'    => 'Theme Settings',
        'redirect'      => false,
        'capability'    => 'edit_posts',
        'menu_slug'     => 'theme-settings',
    ));
    
    // acf_add_options_sub_page(array(
    //     'page_title'    => 'Main Page Settings',
    //     'menu_title'    => 'Main Page',
    //     'parent_slug'   => $general['menu_slug']
    // ));
}


/*SVG support*/
function blog360_svg_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'blog360_svg_types');

/*App2drive ajax*/
function blog360_ajaxurl() {
?>
    <script type="text/javascript">
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
        <?php if( get_field( 'api_key', 'option' ) ) { ?>
            var apiKey = '<?php the_field( 'api_key', 'option' ); ?>';
        <?php } ?>
    </script>
<?php
}
add_action('wp_footer','blog360_ajaxurl');

/*Load posts*/
if (!function_exists('blog360_load_posts')) {
    add_action( 'wp_ajax_blog360_load_posts', 'blog360_load_posts' );
    add_action( 'wp_ajax_nopriv_blog360_load_posts', 'blog360_load_posts' );

    function blog360_load_posts(){
        if( !isset($_POST['category_id']) ) wp_die('id_false');
        $posts_per_page = (int) get_option('posts_per_page');
        $page = $_POST['page'];
        $catID = $_POST['category_id'];
        $searchKey = $_POST['search_key'];

        if( isset($page) ){
            $posts_offset = $page * $posts_per_page;
        }

        $args = array(
            'posts_per_page'        => $posts_per_page,
            'orderby'               => 'date',
            'post_status'           => 'publish'
        );

        if( isset($posts_offset) ){
            $args['offset'] = $posts_offset;
        }

        if( $catID !== '*' ){
            $args['cat'] = $catID;
        }

        if( isset($searchKey) ){
            $args['s'] = $searchKey;
            $args['post_type'] = 'post';
        }

        $query = new WP_Query( $args );
        $max_pages = $query->max_num_pages;

        if ( $query->have_posts() ) :
            while ( $query->have_posts() ) : $query->the_post();
                get_template_part( 'template-parts/post/content' );
            endwhile;
        endif;
        wp_reset_postdata(); 
        wp_die();
    }
}

/*Show partners*/
if (!function_exists('blog360_partners')) {
    function blog360_partners(){ 
        ?>
        <?php if( have_rows('partners') || have_rows('partners', 'option') ): ?>
        <div class="partners">
            <span class="text"><?php echo PARTNERS; ?></span>
            <ul>
                <?php if( have_rows('partners') ) {
                    while ( have_rows('partners') ) : the_row(); ?>
                    <li>
                        <?php if( get_sub_field('link') ) { ?><a href="<?php the_sub_field('link'); ?>" target="_blank"><?php } ?>
                            <?php 
                            if( get_sub_field('logo') ) { ?><img src="<?php echo get_sub_field('logo')['url']; ?>" alt="<?php echo get_sub_field('logo')['url']; ?>"><?php } ?>
                        <?php if( get_sub_field('link') ) { ?></a><?php } ?>
                    </li>
                    <?php endwhile; 
                } elseif( have_rows('partners', 'option' ) ) { 
                    while ( have_rows('partners', 'option') ) : the_row(); ?>
                    <li>
                        <?php if( get_sub_field('link') ) { ?><a href="<?php the_sub_field('link'); ?>" target="_blank"><?php } ?>
                            <?php if( get_sub_field('logo') ) { ?><img src="<?php echo get_sub_field('logo')['url']; ?>" alt=""><?php } ?>
                        <?php if( get_sub_field('link') ) { ?></a><?php } ?>
                    </li>
                    <?php endwhile; 
                } ?>
            </ul>
        </div>
        <?php endif; ?>
    <?php }
}