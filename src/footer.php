<?php
/**
 *
 * @package WordPress
 * @subpackage Blog360
 * @since 1.0
 * @version 1.0
 */
?>  
    </main>
    <footer> 
        <div class="top__line">
            <div class="logo__block float-left">
                <?php if( get_field('footer_logo', 'option') ) { ?>
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo">
                       <img src="<?php the_field('footer_logo', 'option'); ?>" 
                            alt="<?php echo get_option( 'blogname' ); ?>"> 
                    </a>
                <?php } ?>
            </div>
            <?php if( has_nav_menu('footer') ){
                wp_nav_menu( array(
                    'theme_location'        => 'footer',
                    'container'             => 'nav',
                    'container_class'       => 'footer__nav float-left'
                ) );
            } ?>
            <div class="suscribe__block float-right">
                <?php if( get_field('subscribe_title', 'option') ){ ?><h5><?php the_field('subscribe_title', 'option'); ?></h5><?php } ?>
                <?php echo do_shortcode( get_field('subcribe_form_shortcode', 'option') ); ?>
            </div>
        </div>
        <div class="bottom__line">
            <?php if( get_field('copyrights_text', 'option') ){ ?><div class="copyright float-left"><p><?php the_field('copyrights_text', 'option'); ?></p></div><?php } ?>
            <div class="social__block float-right">
                <?php if( get_field('social_links_title', 'option') ){ ?><span class="text"><?php the_field('social_links_title', 'option'); ?></span><?php } ?>
                <nav>
                    <ul>
                        <?php if( get_field('facebook', 'option') ){ ?>
                        <li>
                            <a href="<?php the_field('facebook', 'option'); ?>" target="_blank" class="facebook"></a>
                        </li>
                        <?php } ?>
                        <?php if( get_field('instagram', 'option') ){ ?>
                        <li>
                            <a href="<?php the_field('instagram', 'option'); ?>" target="_blank" class="instagram"></a>
                        </li>
                        <?php } ?>
                        <?php if( get_field('youtube', 'option') ){ ?>
                        <li>
                            <a href="<?php the_field('youtube', 'option'); ?>" target="_blank" class="youtube"></a>
                        </li>
                        <?php } ?>
                        <?php if( get_field('telegram', 'option') ){ ?>
                        <li>
                            <a href="<?php the_field('telegram', 'option'); ?>" target="_blank" class="telegram"></a>
                        </li>
                        <?php } ?>
                        <?php if( get_field('twitter', 'option') ){ ?>
                        <li>
                            <a href="<?php the_field('twitter', 'option'); ?>" target="_blank" class="twitter"></a>
                        </li>
                        <?php } ?>
                    </ul>
                </nav>
            </div>
        </div>
    </footer>
    <?php wp_footer(); ?>

</body>
</html>