<?php
/**
 *
 * @package WordPress
 * @subpackage Blog360
 * @since 1.0
 * @version 1.0
 */
get_header(); 

if( have_rows('content') ):
    while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'posts_slider' ):
            echo '<section class="map__section">';
                get_template_part( 'inc/acf-content/posts-slider' );
            echo '</section>';
        elseif( get_row_layout() == 'image_text_block' ): 
            echo '<section class="about__us padding">';
                get_template_part( 'inc/acf-content/image-text-block' );
            echo '</section>';
        elseif( get_row_layout() == 'youtube_block' ): 
            echo '<section class="subscribe__section">';
                get_template_part( 'inc/acf-content/youtube-block' );
            echo '</section>';
        elseif( get_row_layout() == 'partners_block' ): 
            echo '<section class="partners__section padding__top">';
                get_template_part( 'inc/acf-content/partners-block' );
            echo '</section>';
        endif;
    endwhile;
else :
    echo '
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="no__content">
                        <h1>'.__('Nothing to show', 'blog360').'</h1>
                    </div>
                </div>
            </div>
        </div>
    ';
endif;

get_footer();