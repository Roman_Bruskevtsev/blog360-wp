<?php
/**
 *
 * @package WordPress
 * @subpackage Blog360
 * @since 1.0
 * @version 1.0
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>

</head>
<body <?php body_class();?>>
    <div class="preloader__wrapper">
        <div class="block">
            <span class="square square__1"></span>
            <span class="square square__2"></span>
            <span class="logo"></span>
            <span class="square square__3"></span>
            <span class="square square__4"></span>
        </div>
    </div>
    <header id="header" class="d-none d-xl-block">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo">
                    <?php  
                    if( is_page() || is_home() ) { 
                        $page_logo = get_field('logo');
                        $logo_url = ( $page_logo ) ? get_field('white_logo','option') : get_field('yellow_logo','option');
                        if( $logo_url ) { ?>
                            
                               <img src="<?php echo $logo_url; ?>" 
                                    alt="<?php echo get_option( 'blogname' ); ?>"> 
                            
                        <?php }
                    } 
                    ?>
                    </a>
                    <div class="navigation__row">
                        <?php 
                        $wpml_lang = icl_get_languages();
                        $cur_lang_li = '';
                        $all_lang_li = '';
                        foreach ($wpml_lang as $lang ) {
                            if($lang['active']){
                                $cur_lang_li = '<span class="current__lang">'.$lang['code'].'</span>';
                            } else {
                                $all_lang_li .= '<li>
                                                    <a href="'.$lang['url'].'" class="wpml_'.$lang['code'].'">
                                                '.$lang['code'].'</a>
                                                </li>';
                            }
                        } ?>
                        <div class="lang__switcher float-right">
                            <span class="left__side"></span>
                            <?php echo $cur_lang_li; ?>
                            <ul>
                                <?php echo $all_lang_li; ?>
                            </ul>
                            <span class="right__side"></span>
                        </div>
                        <?php if( get_field('youtube_button_link', 'option') ) { ?>
                            <a href="<?php the_field('youtube_button_link', 'option'); ?>" class="youtube__btn float-right" target="_blank">
                                <span class="border__top"></span>
                                <span class="text"><?php the_field('youtube_button_label', 'option'); ?></span>
                                <span class="border__bottom"></span>
                            </a>
                        <?php } ?>
                        <?php
                        $nav__class = ( get_field('logo') ) ? ' white__nav' : '';
                        if( has_nav_menu('main') ){
                            wp_nav_menu( array(
                                'theme_location'        => 'main',
                                'container'             => 'nav',
                                'container_class'       => 'main__navigation float-right'.$nav__class
                            ) );
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="mobile__nav d-block d-xl-none">
        <?php if( get_field('mobile_logo', 'option') ) { ?>
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="mobile__logo float-left">
           <img src="<?php the_field('mobile_logo', 'option');  ?>" 
                alt="<?php echo get_option( 'blogname' ); ?>"> 
        </a>
        <?php } ?>
        <div class="mobile__btn">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <div class="mobile__navigation d-block d-xl-none">
        <?php if( get_field('mobile_logo', 'option') ) { ?>
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="mobile__logo float-left">
           <img src="<?php the_field('mobile_logo', 'option');  ?>" 
                alt="<?php echo get_option( 'blogname' ); ?>"> 
        </a>
        <?php } ?>
        <div class="lang__switcher float-right">
            <span class="left__side"></span>
            <?php echo $cur_lang_li; ?>
            <ul>
                <?php echo $all_lang_li; ?>
            </ul>
            <span class="right__side"></span>
        </div>
        <div class="close__mobile">
            <span></span>
            <span></span>
            <span></span>
        </div>
        <?php if( has_nav_menu('main') ){
            wp_nav_menu( array(
                'theme_location'        => 'main',
                'container'             => 'nav',
                'container_class'       => 'main__navigation float-right'
            ) );
        } ?>
        <?php if( get_field('youtube_link', 'option') ) { ?>
            <a href="<?php the_field('youtube_link', 'option'); ?>" class="youtube__btn float-left" target="_blank">
                <span class="border__top"></span>
                <span class="text"><?php the_field('youtube_button_label', 'option'); ?></span>
                <span class="border__bottom"></span>
            </a>
        <?php } ?>
    </div>

    <main>